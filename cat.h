///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
/////
///// Usage:  catPower fromValue fromUnit toUnit
/////    fromValue: The number that we want to convert from
/////    fromUnit:  The energy unit of fromValue
/////    toUnit:    The energy unit to convert to
/////
///// Result:
/////   Print out the energy unit conversion
/////
///// Example:
/////   $ ./catPower 3.45e20 e j
/////   3.45E+20 e is 55.2751 j
/////
///// Compilation:
/////   $ g++ -o catPower catPower.cpp
/////   This program will only compile in C++ (with gpp) not in C (with gcc)
/////
///// @file catPower.cpp
///// @version 1.0
/////
///// @see https://en.wikipedia.org/wiki/Units_of_energy
///// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
/////
///// @author Carl Domingo <carld20@hawaii.edu
///// @date   02/13/2022
/////////////////////////////////////////////////////////////////////////////////
#pragma once

const double CATPOWER_IN_A_JOULE = 0.0;
const char CATPOWER              ='c';

extern double fromCatPowerToJoule ( double catPower );
extern double fromJouleToCatPower ( double joule );

